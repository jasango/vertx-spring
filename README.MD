# README #

### Repository Info ###

* vertx-spring
* Version 1.0.0

### Description ###

* Example project with vertx / swagger / spring technologies

### Test Instructions ###

mvn compile exec:java@run

[http://localhost:8082/people](Link URL)
[http://localhost:8082/person/1](Link URL)
[http://localhost:8082/person/2](Link URL)


### Build Instructions ###

sudo docker build -t jasango/vertx-spring .
sudo docker run -d -p 8082:8082 jasango/vertx-spring


### Contact Information ###

* Javier Santana (jasango@gmail.com)