FROM vertx/vertx3	

MAINTAINER Javier Santana "jasango@gmail.com"

ENV LANG C.UTF-8

ENV APP_MAIN org.bitbucket.jasango.vertx.spring.boot.Application
ENV APP_FILE vertx-spring-1.0.0-fat.jar

ENV APP_HOME /usr/verticles

EXPOSE 8082

COPY target/$APP_FILE $APP_HOME/

WORKDIR $APP_HOME
ENTRYPOINT java -Xms64m -Xmx256m -jar $APP_FILE