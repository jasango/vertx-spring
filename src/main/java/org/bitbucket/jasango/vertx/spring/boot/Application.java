package org.bitbucket.jasango.vertx.spring.boot;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.spi.VerticleFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("org.bitbucket.jasango.vertx.spring")
@PropertySource("classpath:application.properties")
public class Application {

	public static void main(String[] args) {
		Vertx vertx = Vertx.vertx();
		ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
		VerticleFactory verticleFactory = context.getBean(SpringVerticleFactory.class);
		vertx.registerVerticleFactory(verticleFactory);
		int instances = ((SpringVerticleFactory) verticleFactory).getVerticleInstances();
		DeploymentOptions options = new DeploymentOptions().setInstances(instances);
		vertx.deployVerticle(verticleFactory.prefix() + ":" + MainVerticle.class.getName(), options);
	}

}
