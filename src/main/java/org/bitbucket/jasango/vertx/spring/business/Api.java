package org.bitbucket.jasango.vertx.spring.business;

import java.util.Collection;

import org.bitbucket.jasango.vertx.spring.domain.Person;
import org.springframework.stereotype.Component;

@Component
public interface Api  {
	
    public Collection<Person> getPeople();
    
    public Person getPerson(String id);
    
}
