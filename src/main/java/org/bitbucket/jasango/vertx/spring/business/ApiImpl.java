package org.bitbucket.jasango.vertx.spring.business;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bitbucket.jasango.vertx.spring.domain.Person;
import org.springframework.stereotype.Component;

@Component
public class ApiImpl implements Api {

	private Map<Integer, Person> people = new HashMap<Integer,Person>();
	
	public ApiImpl() {
		Person p1 = new Person();
		p1.setId(1);
		p1.setName("Miguel");
		Person p2 = new Person();
		p2.setId(2);
		p2.setName("Javier");
		this.people.put(p1.getId(), p1);
		this.people.put(p2.getId(), p2);
	}
	

	@Override
	public Collection<Person> getPeople() {
		System.out.println("getPeople");
		return this.people.values();
		
	}

	@Override
	public Person getPerson(String id) {
		System.out.println("getPerson :" + id);
		return people.get(new Integer(id));
	}

}
