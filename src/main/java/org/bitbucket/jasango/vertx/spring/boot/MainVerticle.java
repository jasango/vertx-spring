package org.bitbucket.jasango.vertx.spring.boot;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.file.FileSystem;
import io.vertx.ext.swagger.router.OperationIdServiceIdResolver;
import io.vertx.ext.swagger.router.SwaggerRouter;
import io.vertx.ext.web.Router;

@Component
@Scope(SCOPE_PROTOTYPE)
public class MainVerticle extends AbstractVerticle {
	private static final Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

	@Value("${vertx.server.serverPort}")
	private int serverPort;

	@Autowired
	private ApiVerticle verticle;
	
	
	@Override
	public void start(Future<Void> startFuture) throws Exception {

		FileSystem vertxFileSystem = vertx.fileSystem();
		vertxFileSystem.readFile("swagger.json", readFile -> {
			if (readFile.succeeded()) {
				Swagger swagger = new SwaggerParser().parse(readFile.result().toString(Charset.forName("utf-8")));
				Router swaggerRouter = SwaggerRouter.swaggerRouter(Router.router(vertx), swagger, vertx.eventBus(), new OperationIdServiceIdResolver());
				deployVerticles(startFuture);
				vertx.createHttpServer().requestHandler(swaggerRouter::accept).listen(serverPort);
				startFuture.complete();
			} else {
				startFuture.fail(readFile.cause());
			}
		});
	}

	public void deployVerticles(Future<Void> startFuture) {

		vertx.deployVerticle(verticle, res -> {
			if (res.succeeded()) {
				LOG.info("ApiVerticle : Deployed");
			} else {
				startFuture.fail(res.cause());
				LOG.error("ApiVerticle : Deployment failed");
			}
		});
	}

}
