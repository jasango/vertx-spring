package org.bitbucket.jasango.vertx.spring.boot;

import io.vertx.core.Verticle;
import io.vertx.core.spi.VerticleFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringVerticleFactory implements VerticleFactory, ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Value("${vertx.server.verticleInstances}")
	private int verticleInstances;

	@Override
	public boolean blockingCreate() {
		return true;
	}

	@Override
	public String prefix() {
		return "Vertx Spring Swagger";
	}

	@Override
	public Verticle createVerticle(String verticleName, ClassLoader classLoader) throws Exception {
		String clazz = VerticleFactory.removePrefix(verticleName);
		return (Verticle) applicationContext.getBean(Class.forName(clazz));
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	public int getVerticleInstances() {
		return verticleInstances;
	}
}
