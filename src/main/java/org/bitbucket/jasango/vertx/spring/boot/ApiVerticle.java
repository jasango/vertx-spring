package org.bitbucket.jasango.vertx.spring.boot;

import org.bitbucket.jasango.vertx.spring.business.Api;
import org.bitbucket.jasango.vertx.spring.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

@Component
public class ApiVerticle extends AbstractVerticle {
	private static final Logger LOG = LoggerFactory.getLogger(ApiVerticle.class);

	@Autowired
	Api service;

	@Override
	public void start() throws Exception {

		vertx.eventBus().<JsonObject>consumer("getPerson").handler(message -> {
			try {
				String id = Json.mapper.readValue(message.body().getString("id"), String.class);
				Person result = service.getPerson(id);
				message.reply(Json.encode(result));
			} catch (Exception e) {
				System.out.println(e.getMessage());
				// TODO : replace magic number (101)
				message.fail(101, e.getLocalizedMessage());
			}
		});

		vertx.eventBus().<JsonObject>consumer("getPeople").handler(message -> {
			try {
				message.reply(new JsonArray(Json.encode(service.getPeople())).encodePrettily());
			} catch (Exception e) {
				// TODO : replace magic number (101)
				message.fail(101, e.getLocalizedMessage());
			}
		});

	}
}
